@extends('admin.layouts.master')
@section('services_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY SERVICES</span>  || <a href="/services/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="9">
									<h2 class='text-center'>Service Information</h2>
                                    </th>	
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Feture image</th>
									<th>Title</th>
									<th>Description</th>
									<th>Integreted topics</th>
									<th>Clinte image</th>
									<th>Clinte feedback</th>
									<th colspan="3">Manage</th>
								</tr>
							</thead>
							<tbody>
									<tr>
										<td>01</td>
										<td>
											<img width="90" height="70" src="../../../assets/images/" alt="No Image"> 
										</td>
										<td>>Title</td>
										<td>
											<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores repellendus minima itaque sunt sed a iusto numquam! Fuga neque, aperiam!....</p>
										</td>
										<td>PHP</td>
										<td>
											<img width="90" height="70" src="../../../assets/images/" alt="No Image"> 
										</td>
										<td>
											<p class="text-justify">....</p>
										</td>
										<td>
											<a class="btn-success" href="/services/show">view</a> 
										</td>									
										<td>
											<a class="btn-success" href="/services/edit">Edit</a> 
										</td>									
										<td>
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/services/trash">Delete</a> 
										</td>
									</tr>
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection
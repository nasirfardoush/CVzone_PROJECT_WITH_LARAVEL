@extends('admin.layouts.master')
@section('services_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">SERVICES - VIEW DETAILS</span>  || <a href="/services">MY SERVICES</a> || <a href="/services/create">ADD NEW</a>
@endsection

@section('content')	
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10"><h2 class="text-center">Service events</h2></th>
								</tr>				
								<tr>
									<th>Service image</th>
									<th>Title</th>
									<th>Integreted topics</th>
									<th>Client image</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
									<img width="90" height="70" src="../../../assets/images/" alt="No Image"> 
									</td>
									<td>Title</td>
									<td>Topics......</td>
									<td>
										<img width="90" height="70" src="../../../assets/images/" alt="No Image"> 
									</td>

								</tr>
								<tr>
									<td  colspan="2">
										<label>Service description :</label>
										<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non voluptatum iste, reiciendis voluptatem, vel consectetur ullam mollitia rem nihil. Mollitia!</p>
									</td>									
									<td class="text-justify" colspan="2">
										<label>Client feedback :</label>
										<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quidem eius omnis tempore alias dolores. Porro alias nisi dolore esse.</p>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<a class="btn-success" href="/services/edit">Edit</a> ||
										<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/services/trash">Delete</a> 
									</td>
								</tr>															
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection	
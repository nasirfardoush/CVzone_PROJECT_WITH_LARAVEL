@extends('admin.layouts.master')
@section('posts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY POSTS</span>  || <a href="/posts/create">ADD NEW</a>
@endsection

@section('content')
<!-- Main content -->
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
   										<h2 class='text-center'>Posts Information</h2>
                                    </th>
								</tr>

								<tr>
									<th>Image</th>
									<th>Title</th>
									<th>Description</th>
									<th>Category name</th>
									<th>Tags</th>
									<th>Author name</th>
									<th>City</th>
									<th>Country</th>
									<th colspan="3">Manage</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><img  width="60" height="50" src="../../../assets/images/" alt="Image" > </td>
									<td>Learn PHP here</td>
									<td >
										<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa dolor, accusamus, vitae ullam nostrum quos voluptas fugiat blanditiis. Officiis, consequuntur.....</p>
									</td>
									<td>Education</td>
									<td>PHP</td>
									<td>Nasir</td>
									<td>Dhaka</td>
									<td>Bangladesh</td>
									<td>
                                        <a class="btn-success" href="/posts/show">View</a>
									</td>
									<td>
                                        <a class="btn-success" href="/posts/edit">Edit</a>
                                      </td>  
									<td>
										<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/posts/trash">Delete</a>
									</td>
								</tr>								


							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->
@endsection
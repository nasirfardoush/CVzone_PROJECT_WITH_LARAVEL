@extends('admin.layouts.master')
@section('posts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">POSTS - EDIT</span> || <a href="/posts">MY POSTS</a> || <a href="/posts/create">ADD NEW</a>
@endsection

@section('content')
    <div class="row ">
        <!-- about basic info about module -->
        <form action="update.php" method="POST" enctype="multipart/form-data">
            <fieldset class="content-group">
                <div class="form-group">
                    <div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
                         <h5>You can Update  your post .</h5>
                            <!-- section one -->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Posts Title</label>
                                    <input class="form-control " type="tel" placeholder="Learn PHP" value="" name="title">
                                </div>
                                <div class="form-group">
                                    <label>Author name</label>
                                    <input  class="form-control" type="text" placeholder="Rahim" value="" name="author_name">
                                </div>
                                <div class="form-group">
                                    <label>Categoty name</label>
                                    <input  class="form-control" type="text" placeholder="Education" value="" name="categories">
                                </div>

                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control input-xlg"  placeholder="Description"  name="description" ><</textarea>
                                </div>
                            </div>
                            <!-- Second section -->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Country Name</label>
                                    <input class="form-control" type="text" placeholder="Dhaka" value=" " name="country_name">
                                </div>
                                <div class="form-group">
                                    <label>City name</label>
                                    <input class="form-control" type="text" placeholder="Bangladesh" value="" name="city_name">
                                </div>
                                <div class="form-group">
                                    <label>Tags</label>
                                    <input class="form-control" type="text" placeholder="php" value="" name="tags">
                                </div>                                
                                <div class="form-group">
                                    <label>Post Image</label>
                                    <input class="form-control" type="file"  name="img">
                                    <img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/" alt="No Image">

                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" value="" name="id">
                            <input type="submit" value="Update" name="aboutupdate">
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
@endsection
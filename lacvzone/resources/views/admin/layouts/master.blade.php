{{-- Include header section --}}
@include('admin.layouts.include.header')
<!-- Main sidebar -->
<div class="sidebar sidebar-main">
	<div class="sidebar-content">
		@include('admin.layouts.include.userMenu')
		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
					<li class="@yield('base_menu_dashboard')"><a href="/admin/dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>								
					<li>
						<a   href="#"><i class="icon-stack2"></i> <span>SETTINGS</span></a>
						<ul>									
							<li class="@yield('settings_menu_manage')">
								<a  href="/settings">Manage Account Settings</a>
							</li>										
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>ABOUT</span></a>
						<ul>									
							<li class="@yield('abouts_menu_manage')">
								<a href="/abouts">Manage Abouts</a>
							</li>										
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>HOBBIES</span></a>
						<ul>
							<li class="@yield('hobbies_menu_add')">
								<a href="/hobbies/create">Add new</a>
							</li>										
							<li class="@yield('hobbies_menu_manage')">
								<a href="/hobbies">Manage Hobbies</a>
							</li>										
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>FACTS</span></a>
						<ul>
							<li class="@yield('facts_menu_add')">
								<a href="../facts/create">Add new</a>
							</li>										
							<li class="@yield('facts_menu_manage')">
								<a href="../facts">Manage Facts</a>
							</li>										
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>EDUCATIONS</span></a>
						<ul>
							<li class="@yield('educations_menu_add')">
								<a href="/educations/create">Add new</a>
							</li>
							<li class="@yield('educations_menu_manage')">
								<a href="/educations">Manage Educations</a>
							</li>
						</ul>
					</li>									
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>EXPERIENCE</span></a>
						<ul>
							<li class="@yield('experiences_menu_add')">
								<a href="/experiences/create">Add new</a>
							</li>
							<li class="@yield('experiences_menu_manage')">
								<a href="/experiences">Manage exeperiences</a>
							</li>
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>AWARDS</span></a>
						<ul>
							<li class="@yield('awards_menu_add')">
								<a href="/awards/create">Add new</a>
							</li>
							<li class="@yield('awards_menu_manage')">
								<a href="/awards">Manage Awards</a>
							</li>
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>POST</span></a>
						<ul>
							<li class="@yield('posts_menu_add')"><a href="/posts/create">Add new</a></li>
							<li class="@yield('posts_menu_manage')"><a href="/posts">Manage Post</a></li>
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>SERVICES</span></a>
						<ul>
							<li class="@yield('services_menu_add')">
								<a href="../services/create">Add New</a>
							</li>										
							<li class="@yield('services_menu_add')">
								<a href="../services">Manage services</a>
							</li>
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>TEACHING</span></a>
						<ul>
							<li class="@yield('teaching_menu_add')">
								<a href="/teaching/create">Add New</a>
							</li>										
							<li class="@yield('teaching_menu_manage')">
								<a href="/teaching">Manage Teaching</a>
							</li>
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>SKILLS</span></a>
						<ul>
							<li class="@yield('skills_menu_add')">
								<a href="/skills/create">Add New</a>
							</li>
							<li class="@yield('skills_menu_manage')">
								<a href="/skills">Manage Skills</a>
							</li>
						</ul>
					</li>								
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>PORTFOLIO</span></a>
						<ul>
							<li class="@yield('portfolios_menu_add')""><a href="/portfolios/create">Add New</a></li>
							<li class="@yield('portfolios_menu_manage')"><a href="/portfolios">Manage portfolio</a></li>
						</ul>
					</li>								
					<li class="@yield('contacts_menu_manage')">
						<a href="/contacts"><i class="icon-stack2"></i> <span>CONTACT</span></a>

					</li>

				</ul>
			</div>
		</div>
		<!-- /main navigation -->
	</div>
</div>
			<!-- /main sidebar -->

<!-- Main content -->
<div class="content-wrapper">

	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> 
					@yield('pageTitle')	
				</h4>
			</div>
		</div>
	</div>	
	@yield('content')		
</div>	
<!-- /main content -->
{{-- Include footer section --}}
@include('admin.layouts.include.footer')
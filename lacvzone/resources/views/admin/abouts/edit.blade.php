@extends('admin.layouts.master')
@section('abouts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">ABOUT EDIT</span>  | <a href="/abouts"> ABOUT ME</a>
@endsection

@section('content')
<!-- Main content -->	
<div class="row ">
    <!-- about basic info about module -->
	<form action="update.php" method="POST">
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">							
							<div class="form-group">
								<label>Phone number</label>
								<input class="form-control input-xlg" type="tel" value="01753736374" name="phone">
							</div>					
							<div class="form-group">
								<label>Skills work area</label>
								<input class="form-control input-xlg" type="text" value="Developer,Designer" name="work_area">
								<small>Add another work area and separeted with comma(,) .</small>
							</div>

							<div class="form-group">
								<label>Sort description about you</label>
								<input class="form-control input-lg" type="text" value="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, ea." name="short_desc">
							</div>									
							<div class="form-group">
								<label>BIO</label>
								<textarea class="form-control input-lg" name="bio"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum nam blanditiis, illo officiis libero similique sint ad ipsa, magnam totam!</textarea>
							</div>
								
							<div class="form-group">
								<input type="hidden" value="" name="id">
								<input type="submit" value="Udate" name="aboutupdate">
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>
@endsection	 
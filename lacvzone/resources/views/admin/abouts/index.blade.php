@extends('admin.layouts.master')
@section('abouts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">ABOUT ME</span>  | <a href="/abouts/edit"> UPDATE</a>
@endsection

@section('content')
<!-- Main content -->

	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="5">
										<h2 class="text-center">About Information</h2>
									</th>
								</tr>				
								<tr>
									<th>Phone</th>
									<th>Skills tag</th>
									<th>Sort description</th>
									<th>BIO</th>
									<th >Manage</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>01753736374</td>
									<td>Designer,Developer</td>
									<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, labore!</td>
									<td >
										<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque nulla magnam voluptatibus similique quos unde. Perspiciatis ex laboriosam molestias, architecto.</p>
									</td>
									<td>
										<a class="btn-success" href="/abouts/edit">Edit</a> 
									</td>									
								</tr>

							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->
@endsection

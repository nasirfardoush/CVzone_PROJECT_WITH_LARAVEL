@extends('admin.layouts.master')
@section('experiences_menu_add','active')
@section('pageTitle')
<span class="text-semibold">EXPERIENCE - ADD</span>  || <a href="/experiences">MY EXPERIENCE</a>
@endsection

@section('content')
	<div class="row ">  
		    <!-- about basic info about module -->
			<form action="store.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
							<h4>Add your experiences .</h4>								
								<!-- section one -->
									<div class="col-md-5">
										<div class="form-group">
											<label>Designation</label>
											<input class="form-control " type="tel" placeholder="Eb Developer" name="designation">
										</div>					
										<div class="form-group">
											<label>Company Name</label>
											<input class="form-control " type="text" placeholder="Webtech" name="company_name">
										</div>
										<div class="form-group">
											<label>Company Location</label>
											<input class="form-control " type="text" placeholder="Bangladesh" name="company_location">
										</div>
										<div class="form-group">
											<label>Sort description</label>
											<textarea class="form-control"  placeholder="" name="expreince_desc"></textarea>
										</div>									
									</div>
									<!-- Section Two -->									
									<div class="col-md-5">				
										<div class="form-group">
											<label>Start Year</label>
											<input id="datepicker" class="form-control" type="text" placeholder="2001" name="start_date">
										</div>	
										<div class="form-group">
											<label>End Year</label>
											<input id="datepicker2" class="form-control" type="text" placeholder="2005" name="end_date">
								     	</div>																									
								 </div>	
							</div>
							<div class="form-group ">
								<input class="marg-top" type="submit" value="Save" name="experiences">
							</div>	
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 
@endsection
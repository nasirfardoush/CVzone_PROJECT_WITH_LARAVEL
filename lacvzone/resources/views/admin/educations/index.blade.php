@extends('admin.layouts.master')
@section('educations_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY EDUCATIONS</span>  || <a href="/educations/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
	 <div class="table-responsive">
		<table class="table bg-slate-600">
			<thead>
				<tr>
					<th colspan="12"><h2 class="text-center">Education Information</h2></th>
				</tr>				
				<tr>
					<th>Sl no</th>
					<th>Title</th>
					<th>Degree</th>
					<th>Institute</th>
					<th>Institute location</th>
					<th>Enrolled year</th>
					<th>Pasing year</th>
					<th>Result(GPA)</th>
					<th>Duration</th>
					<th>Board</th>
					<th colspan="2">Manage</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>01</td>
					<td>Title</td>
					<td>Bsc</td>
					<td>DU</td>
					<td>Dhaka</td>
					<td>2013</td>
					<td>2016</td>
					<td>3.80</td>
					<td>4</td>
					<td>NA</td>
					<td>
						<a class="btn-success" href="/educations/edit">Edit</a> 
					</td>									
					<td>
						<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/educations/trash">Delete</a> 
					</td>
				</tr>														
			</tbody>
		</table>
	 </div>
 </div>
</div> 	
@endsection
@extends('admin.layouts.master')
@section('educations_menu_add','active')
@section('pageTitle')
<span class="text-semibold">EDUCATIONS - ADD</span>  || <a href="/educations">MY EDUCATIONS</a>
@endsection

@section('content')
	<div class="row ">
		<form action="store.php" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
						<div class="row">
						<h5>Please add your educations here .</h5>
							<!-- section one -->
							<div class="col-md-5">
								<div class="form-group">
									<label>Educations Title</label>
									<input class="form-control" type="tel" placeholder="Computer engineering" name="title">
								</div>					
								<div class="form-group">
									<label>Institute Name</label>
									<input class="form-control" type="text" placeholder="University of Dhaka" name="institute">
								</div>

								<div class="form-group">
									<label>Enrolled Year</label>
									<input id="datepicker" class="form-control" type="text" placeholder="2001" name="enrolled_year">
								</div>									
								<div class="form-group">
									<label>Result</label>
									<input class="form-control" type="text" placeholder="4.00" name="result">
									<small>Enter result following GPA Standart</small>
								</div>									
								<div class="form-group">
									<label>Board</label>
									<input class="form-control" type="text" placeholder="Technical" name="education_board">
								</div>
							</div>								
							<!-- Second section -->							
							<div class="col-md-5">
								<div class="form-group">
									<label>Educations Degree</label>
									<input class="form-control" type="tel" placeholder="MSC or BSc..." name="degree">
									<small>Please write sort name.</small>
								</div>					
								<div class="form-group">
									<label>Institute Location</label>
									<input class="form-control" type="text" placeholder="Bangladesh" name="location">
								</div>

								<div class="form-group">
									<label>Passing Year</label>
									<input id="datepicker2" class="form-control" type="text" placeholder="2005" name="passing_year">
								</div>									
								<div class="form-group">
									<label>Course duration(Years)</label>
									<input class="form-control" type="text" placeholder="4" name="course_duration">
								</div>																	
							</div>
						</div>
						<div class="form-group">
							<input class="marg-top" type="submit" value="Save" name="education">
						</div>
					</div>
				</div>
			</fieldset>
		</form>	
   	</div>
@endsection
@extends('admin.layouts.master')
@section('awards_menu_add','active')
@section('pageTitle')
<span class="text-semibold">AWARDS - ADD</span>  || <a href="/awards">MY AWARDS</a>
@endsection

@section('content')
<div class="row ">
	<!-- about basic info about module -->
	<form action="store.php" method="POST" enctype="multipart/form-data">
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
					<div class="row">
					<h5>Please add your awards here .</h5>
						<!-- section one -->
						<div class="col-md-5">
							<div class="form-group">
								<label>Awards Title</label>
								<input class="form-control input-xlg" type="tel" placeholder="Eb Developer" name="title">
							</div>			
							<div class="form-group">
								<label>Awards Year</label>
								<input id="datepicker" class="form-control" type="text" placeholder="2001" name="year">
							</div>										
							<div class="form-group">
								<label>Sort description</label>
								<textarea class="form-control"  placeholder="" name="description"></textarea>
							</div>									
						</div>								
						<!-- Second section -->							
						<div class="col-md-5">
							<div class="form-group">
								<label>Organaizations Name</label>
								<input class="form-control input-xlg" type="text" placeholder="Webtech" name="organization">
							</div>					
							<div class="form-group">
								<label>Organaization Location</label>
								<input class="form-control input-xlg" type="text" placeholder="Bangladesh" name="location">
							</div>					
						</div>
					</div>
					<div class="form-group">
						<input class="marg-top" type="submit" value="Save" name="awards">
					</div>
				</div>
			</div>
		</fieldset>
	</form>	
</div>
@endsection
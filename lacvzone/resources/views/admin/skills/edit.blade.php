@extends('admin.layouts.master')
@section('skills_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">SKILLS - EDIT</span>  || <a href="/skills">MY SKILLS</a> || <a href="/skills/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row ">
  
		    <!-- Teaching Module -->
			<form action="update.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
							 <h5>Yo can  update your skills .</h5>
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Title</label>
										<input class="form-control" type="title" value="Title" name="title">
									</div>	
									<div class="form-group">
										<label>Skills area</label>
										<textarea class="form-control"  name="experience_area">
											Lorem ispum
										</textarea>
										<small>Please separet your skills with comma(,)</small>
									</div>									
									<div class="form-group">
										<label>Sort description</label>
										<textarea class="form-control"   name="description">
											value=""
										</textarea>
									</div>
								</div>																		
									<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Expreince(Years)</label>
										<input class="form-control" type="text" value="" name="experience">
									</div>						
								     <div class="form-group">
											<label>Skills level</label>
											<select class="form-control" name="level">
												
												<option value="">
												</option>
												
												<option value="intermediate">Intermediate</option>
												<option value="Experts">Experts</option>
												<option value="Advance">Advance</option>
												<option value="Master">Master</option>
											</select>
								     	</div>										
								     	<div class="form-group">
											<label>Skills category</label>
											<select class="form-control" name="category" >
												
												<option value=""> 
												</option>
																				
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
											</select>
								     	</div>						
								</div>
							</div>
							<div class="form-group">
							
                          
									<input  type="hidden" value="" name="id">
									<input class="marg-top" type="submit" value="Update" name="skills">
							</div>
						</div>
					</div>	
				</fieldset>
			</form>	
   		 </div>
  </div> 
@endsection
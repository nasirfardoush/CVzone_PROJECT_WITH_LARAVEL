@extends('admin.layouts.master')
@section('skills_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY SKILLS</span>  || <a href="/skills/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
									<h2 class='text-center'>Skill Information</h2>
								  </th>
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Title</th>
									<th>Skill area</th>
									<th>Description</th>
									<th>Experience</th>
									<th>Level</th>
									<th>Category</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
									<tr>
										<td>01</td>
										<td>Title</td>
										<td>experience_area</td>
										<td>
											<p class="text-justify">description</p>
										</td>
										<td>4 years
										</td>
										<td>hh</td>
										<td>hh</td>
										<td>
											<a class="btn-success" href="/skills/edit">Edit</a> 
										</td>									
										<td>
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/skills/trash">Delete</a> 
										</td>
									</tr>							 																			
							</tbody>
						</table>
				</div>
		 </div>
	</div>		
@endsection
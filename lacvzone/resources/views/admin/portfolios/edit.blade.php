@extends('admin.layouts.master')
@section('portfolios_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">PORTFOLIOS - EDIT</span>  || <a href="/portfolios">MY PORTFOLIOS</a> || <a href="/portfolios/create">ADD NEW</a>
@endsection

@section('content')
<div class="row" >
		<form action="update.php" method="POST" enctype="multipart/form-data">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
							 <h5>Please add your facts here .</h5>
								<div class="form-group">
									<label>Project Title</label>
									<input class="form-control" type="text" value="" name="title">
								</div>														
								<div class="form-group">
									<label>Category</label>
									<input class="form-control" type="text" value="" name="category">

								</div>								
								<div class="form-group">
									<label>Project description</label>
									<textarea class="form-control input-xlg" name="description" >
										
									</textarea>
								</div>
								<div class="form-group">
									<label>Project image</label>
									<input class="form-control" type="file" name="img" >
									<img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/" alt="No Image">
								</div>	
								
								<div class="form-group">
									<input class="input-xs" type="hidden" value="" name="id">
									<input class="input-xs" type="submit" value="Update" name="protfolios">

								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
</div>		
@endsection
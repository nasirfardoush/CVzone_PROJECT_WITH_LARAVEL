@extends('admin.layouts.master')
@section('teaching_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY TEACHING</span>  || <a href="/teaching/create">ADD NEW</a>
@endsection
@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
										<h2 class='text-center'>Teaching Information</h2>
									</th>
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Title</th>
									<th>Institute</th>
									<th>Start year</th>
									<th>End year</th>
									<th>Description</th>
									<th>Status</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
									<tr>
										<td>01</td>
										<td>Profesor</td>
										<td>DU</td>
										<td>2011</td>
										<td>2017</td>
										<td>
											<p class="text-justify">Lorem ispum hello</p>
										</td>
										<td>Curent</td>
										<td>
											<a class="btn-success" href="/teaching/edit">Edit</a> 
										</td>									
										<td>
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/teaching/trash">Delete</a> 
										</td>
									</tr>															
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection
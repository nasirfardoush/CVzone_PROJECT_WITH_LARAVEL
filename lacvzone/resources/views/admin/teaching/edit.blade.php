@extends('admin.layouts.master')
@section('teaching_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">TEACHING - EDIT</span>  || <a href="/teaching">MY TEACHING</a> || <a href="/teaching/create">ADD NEW</a>
@endsection

@section('content')
    <div class="row ">

        <!-- Teaching Module -->
        <form action="update.php" method="POST">
            <fieldset class="content-group">
                <div class="form-group">
                    <div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
							<h5>You can update  your teaching .</h5>
                            <!-- section one -->
                            <div class="col-md-5">
                                <div class="form-group">

                                    <label>Title</label>
                                    <input class="form-control" type="text" placeholder="Professor"  value="title" name="title">
                                </div>
                                <div class="form-group">
                                    <label>Start Year</label>
                                    <input id="datepicker" class="form-control" type="text"  value="" placeholder="2001" name="start_date">
                                </div>
                                <div class="form-group">
                                    <label>Sort description</label>
                                    <textarea class="form-control"  placeholder=""  name="teaching_desc">Lorem ispum something else </textarea>
                                </div>
                            </div>
                            <!-- Second section -->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Institute Name</label>
                                    <input class="form-control" type="text" placeholder="Dhaka university"  value="" name="institute">
                                </div>
                                <div class="form-group">
                                    <label>End Year</label>
                                    <input id="datepicker2" class="form-control" type="text" placeholder="2010"  value="" name="end_date">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control"  name="teaching_status">
                                       	Teaching status
                                        <option value="">
                                        </option>
                                                                          
                                        <option name="teaching_status"  value="CURRENT">Curent</option>
                                        <option name="teaching_status" value="PREVIOUS">Privious</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" value="" name="id">
                            <input class="marg-top" type="submit" value="Update" name="teaching">
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
@endsection
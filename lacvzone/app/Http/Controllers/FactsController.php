<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FactsController extends Controller
{
    public function index(){
    	return view('admin.facts.index');
    }    
    public function create(){
    	return view('admin.facts.create');
    }   
     public function edit(){
     	return view('admin.facts.edit');
    }
}

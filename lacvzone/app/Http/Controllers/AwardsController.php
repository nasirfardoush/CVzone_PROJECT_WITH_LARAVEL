<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AwardsController extends Controller
{
    public function index(){
    	 return view('admin.awards.index');
    }    
    public function create(){
    	  return view('admin.awards.create');
    }    
    public function edit(){
    	  return view('admin.awards.edit');
    }
}

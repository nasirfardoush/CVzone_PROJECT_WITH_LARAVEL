<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExperiencesController extends Controller
{
    public function index()
    {
    	 return view('admin.experiences.index');
    }    
    public function create()
    {
    	 return view('admin.experiences.create');
    }    
    public function edit()
    {
    	 return view('admin.experiences.edit');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EducationsController extends Controller
{
    public function index(){
    	return view('admin.educations.index');
    }   

     public function create(){
    	return view('admin.educations.create');
    }  
      
    public function edit(){
    	return view('admin.educations.edit');
    }
}
